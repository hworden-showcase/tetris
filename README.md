# Tetris

A simple implementation of the classic game of Tetris.
Written entirely in JavaScript and HTML, leveraging the HTML5 canvas element.

## Controls
    - A - left
    - D - right
    - S - down
    - E - rotate right
    - Q - rotate left
    - P - pause

[Play now!](https://hworden-showcase.gitlab.io/tetris/index.html)
