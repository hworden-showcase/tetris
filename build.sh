#! /bin/#!/bin/sh

rm -rf public/
mkdir public/
cp src/index.html public/index.html
npx lessc src/app.less public/app.css
npx webpack --config public.config.js
