import "babel-polyfill";
import ColorGrid from "./modules/ColorGrid.js";
import {Actions, TetrisReducer} from "./modules/TetrisReducer.js";
import Piece from './modules/Piece.js';
import Board from './modules/Board.js';

const canvas = document.getElementById('board');
const overlay = document.getElementById('overlay');
const ptsDisplay = document.getElementById('ptsDisplay');
const overlayText = document.getElementById('overlayText');
const widthScale = 0.5;

function resize() {
  canvas.width = widthScale*canvas.clientHeight;
  canvas.height = canvas.clientHeight;
}

function showOverlay(show) {
  if (show) {
    overlay.style = '';
  }
  else {
    overlay.style = 'display: none;';
  }
}

function setOverlayText(color, txt) {
  overlayText.style = `color: ${color};`;
  overlayText.innerHTML = txt;
}

function updatePoints(pts) {
  ptsDisplay.innerHTML = pts + ' pts'
}

function Tetris() {
  resize();
  const colorGrid = new ColorGrid(canvas, 10, 20);
  const board = new Board(10, 20);

  const o = new Piece('orange', [
    [1, 1],
    [1, 1]
  ]);

  const t = new Piece('purple', [
    [0, 0, 0],
    [0, 1, 0],
    [1, 1, 1]
  ]);

  const l = new Piece('orange', [
    [0, 0, 0],
    [0, 0, 1],
    [1, 1, 1],
  ]);

  const j = new Piece('blue', [
    [0, 0, 0],
    [1, 0, 0],
    [1, 1, 1],
  ]);

  const z = new Piece('red', [
    [0, 0, 0],
    [1, 1, 0],
    [0, 1, 1]
  ]);

  const s = new Piece('green', [
    [0, 0, 0],
    [0, 1, 1],
    [1, 1, 0]
  ]);

  const i = new Piece('cyan', [
    [0, 0, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 0]
  ]);

  const pieces = [o, t, l, j, z, s, i];
  const randomPiece = function () {
    return pieces[Math.floor((Math.random() * 100000)) % pieces.length];
  }

  const redraw = () => {
    const state = store.getState();
    if (state.finished) setOverlayText('red', 'Game Over!');
    else if (state.paused) setOverlayText('white', 'Paused...');
    showOverlay(state.finished || state.paused);
    colorGrid.clear();
    for (const cell of state.piece.getCells(state.x, state.y, state.r)) {
      colorGrid.setColor(cell.x, cell.y, state.piece.color());
    }
    for (const cell of board.occupiedCells()) {
      colorGrid.setColor(cell.x, cell.y, cell.clr);
    }
    updatePoints(state.points);
  }

  const initStProvider = function() {
    return {
      x: 0,
      y: 0,
      r: 0,
      piece: randomPiece(),
      paused: false,
      points: 0,
      finished: false
    };
  }
  const store = new Store(initStProvider(), TetrisReducer(initStProvider, board));
  store.subscribe(() => {
    window.requestAnimationFrame(redraw);
  });

  window.addEventListener("keypress", (evt) => {
    if (event.defaultPrevented) return;
    if (evt.key == "s") store.dispatch(Actions.PLAYER_DOWN);
    if (evt.key == "d") store.dispatch(Actions.PLAYER_RIGHT);
    if (evt.key == "a") store.dispatch(Actions.PLAYER_LEFT);
    if (evt.key == "e") store.dispatch(Actions.PLAYER_ROTATE_RIGHT);
    if (evt.key == "q") store.dispatch(Actions.PLAYER_ROTATE_LEFT);
    if (evt.key == "p") store.dispatch(Actions.GAME_PAUSE);
  });

  window.setInterval(() => {
    const state = store.getState();
    if (!state.paused && !state.finished) store.dispatch(Actions.PLAYER_DOWN);
  }, 750);
}
window.onload = Tetris;

class Store {
  constructor(initialState, reducer) {
    this.state = initialState;
    this.reducer = reducer;
    this.subscribers = [];
  }

  getState() {
    return this.state;
  }

  dispatch(action) {
    this.state = this.reducer(this.state, action);
    this.subscribers.forEach(handler => handler());
  }

  subscribe(handler) {
    this.subscribers.push(handler);
  }

  unsubscribe(handler) {
    const index = this.subscribers.indexOf(handler);
    if (index > 0) {
      this.subscribers.splice(index, 1);
    }
  }
}
