import {Actions, TetrisReducer} from "../modules/TetrisReducer.js"
import Piece from "../modules/Piece.js"
import Board from "../modules/Board.js"
import assert from "assert"

describe('TetrisReducer', () => {
  /* Single cell piece for testing */
  const testPiece = new Piece('orange', [[1]]);
  /* Small board for testing */
  const testBoard = new Board(3,3);
  describe('Movement', () => {
    let unitUnderTest;
    const initStateProvider = () => {
      return {
        x: 0,
        y: 0,
        r: 0,
        piece: testPiece,
        paused: false,
        points: 0,
        alive: true
      };
    }
    beforeEach(() => {
      testBoard.occupiedCells().forEach(p => {
        testBoard.clear(p.x, p.y);
      });
      unitUnderTest = new TetrisReducer(initStateProvider, testBoard);
    });
    describe('Bounds checks', () => {
      it('left stays put', () => {
        const beforeState = {...initStateProvider(), x:0, y:0};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_LEFT);
        assert.equal(beforeState.x, afterState.x, 'x should not move');
        assert.equal(beforeState.y, afterState.y, 'y should not move');
      });
      it('right stays put', () => {
        const beforeState = {...initStateProvider(), x:testBoard.width-1, y:0};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_RIGHT);
        assert.equal(beforeState.x, afterState.x, 'x should not move');
        assert.equal(beforeState.y, afterState.y, 'y should not move');
      });
      it('down resets piece', () => {
        const initialState = initStateProvider();
        const beforeState = {...initialState, x:0, y:testBoard.height-1};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_DOWN);
        assert.equal(afterState.x, initialState.x, 'x should be initial');
        assert.equal(afterState.y, initialState.y, 'y should be initial');
        assert.notEqual(beforeState.y, initialState.y, 'y should have reset');
      });
    });
    describe('Rotation', () => {
      it('left', () => {
        const beforeState = {...initStateProvider(), x:0, y:0, r: 1};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_ROTATE_LEFT);
        assert.equal(beforeState.x, afterState.x, 'x should not move');
        assert.equal(beforeState.y, afterState.y, 'y should not move');
        assert.equal(afterState.r, 0, 'Go back 1');
      });

      it('left reset', () => {
        const beforeState = {...initStateProvider(), x:0, y:0};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_ROTATE_LEFT);
        assert.equal(beforeState.x, afterState.x, 'x should not move');
        assert.equal(beforeState.y, afterState.y, 'y should not move');
        assert.equal(afterState.r, 3, 'Go back 1');
      });

      it('right', () => {
        const beforeState = {...initStateProvider(), x:0, y:0, r:0};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_ROTATE_RIGHT);
        assert.equal(beforeState.x, afterState.x, 'x should not move');
        assert.equal(beforeState.y, afterState.y, 'y should not move');
        assert.equal(afterState.r, 1, 'Go forward 1');
      });

      it('right reset', () => {
        const beforeState = {...initStateProvider(), x:0, y:0, r:3};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_ROTATE_RIGHT);
        assert.equal(beforeState.x, afterState.x, 'x should not move');
        assert.equal(beforeState.y, afterState.y, 'y should not move');
        assert.equal(afterState.r, 0, 'Go forward 1');
      });
    });
    describe('Collision', () => {
      beforeEach(() => {
        /* Set a block in the middle to collide with */
        testBoard.set(1, 1, 'black');
        unitUnderTest = new TetrisReducer(initStateProvider, testBoard);
      });
      it('left stays put', () => {
        const beforeState = {...initStateProvider(), x:testBoard.width-1, y:1};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_LEFT);
        assert.equal(afterState.x, beforeState.x, 'x should not move');
        assert.equal(afterState.y, beforeState.y, 'y should not move');
      });
      it('right stays put', () => {
        const beforeState = {...initStateProvider(), x:0, y:1};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_RIGHT);
        assert.equal(afterState.x, beforeState.x, 'x should not move');
        assert.equal(afterState.y, beforeState.y, 'y should not move');
      });
      it('down resets piece', () => {
        const initialState = initStateProvider();
        const beforeState = {...initialState, x:1, y:0};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_DOWN);
        assert.equal(afterState.x, initialState.x, 'x should be initial');
        assert.equal(afterState.y, initialState.y, 'y should be initial');
      });
    });
    describe('Gameplay', () => {
      beforeEach(() => {
        /* Set a block in the middle to collide with */
        testBoard.set(1, 1, 'black');
        unitUnderTest = new TetrisReducer(initStateProvider, testBoard);
      });
      it('pause', () => {
        const beforeState = {...initStateProvider(), paused: false};
        const afterState = unitUnderTest(beforeState, Actions.GAME_PAUSE);
        assert.equal(afterState.paused, true);
      });
      it('unpause', () => {
        const beforeState = {...initStateProvider(), paused: true};
        const afterState = unitUnderTest(beforeState, Actions.GAME_PAUSE);
        assert.equal(afterState.paused, false);
      });
      it('game over', () => {
        testBoard.set(0, 0, 'black');
        const beforeState = {...initStateProvider()};
        const afterState = unitUnderTest(beforeState, Actions.PLAYER_DOWN);
        assert.equal(afterState.finished, true, 'should have finished');
      })
    });
  });
});
