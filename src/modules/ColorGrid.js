export default class ColorGrid {
  constructor(canvas, gridWidth, gridHeight) {
    this.gridHeight = gridHeight;
    this.gridWidth = gridWidth;
    this.canvas = canvas;
    this.drawCtx = canvas.getContext('2d');
  }

  setColor(x, y, color) {
    const {width, height} = this.getCellDims();
    x = width * x;
    y = height * y;
    this.drawCtx.fillStyle = color;
    this.drawCtx.fillRect(x, y, width, height);
    this.drawCtx.shadowColor = 'transparent';
    this.drawCtx.beginPath();
    this.drawCtx.lineWidth = "1px";
    this.drawCtx.strokeStyle = "rgb(0,0,0,1)";
    this.drawCtx.strokeRect(x, y, width, height);
    this.drawCtx.strokeRect(x, y, width, height);
  }

  clear() {
    this.drawCtx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  getCellDims() {
    const {width, height} = this.canvas;
    return {
      width: width / this.gridWidth,
      height: height / this.gridHeight
    }
  }
}
