export default class Board {
  constructor(width, height) {
    this.board = [];
    this.width = width;
    this.height = height;
    for (let h = 0; h < height; h++) {
      this.addEmptyRow();
    }
  }

  addEmptyRow() {
    this.board.unshift([]);
    for (let w = 0; w < this.width; w++) {
      this.board[0].push(null);
    }
  }

  available(x, y) {
    return this.inbounds(x, y) && this.color(x, y) === null;
  }

  color(x, y) {
    return this.board[y][x];
  }

  inbounds(x, y) {
    return x >= 0 && x < this.width && y >= 0 && y < this.height;
  }

  set(x, y, color) {
    this.board[y][x] = color;
  }

  clear(x, y) {
    this.set(x, y, null);
  }

  deleteRow(y) {
    this.board.splice(y, 1);
    this.addEmptyRow();
  }

  rowFull(y) {
    return this.board[y].reduce((tot, val) => {
      return val == null ? tot + 0 : tot + 1;
    }, 0) === this.width;
  }

  occupiedCells() {
    const cells = [];
    for (let h = 0; h < this.height; h++) {
      for (let w = 0; w < this.width; w++) {
        if (!this.available(w, h)) {
          cells.push({ 'x': w, 'y': h, 'clr': this.color(w,h)});
        }
      }
    }
    return cells;
  }
}
