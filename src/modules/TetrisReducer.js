let _action = 0;

export const Actions = {
  PLAYER_DOWN: _action++,
  PLAYER_LEFT: _action++,
  PLAYER_ROTATE_LEFT: _action++,
  PLAYER_RIGHT: _action++,
  PLAYER_ROTATE_RIGHT: _action++,
  GAME_PAUSE: _action++
};

export const TetrisReducer = function(stateProvider, board) {
  return function (state = initialState, action) {
    if (state.piece.getCells(state.x, state.y, state.r)
      .some(cell => !board.available(cell.x, cell.y))) {
        return {...state, finished: true};
    }
    let newState;
    if (action == Actions.GAME_PAUSE) return {...state, paused: !state.paused};
    if (state.paused) return {...state};
    switch (action) {
      case Actions.PLAYER_RIGHT:
        newState = {...state, x: state.x+1};
        break;
      case Actions.PLAYER_LEFT:
        newState = {...state, x: state.x-1};
        break;
      case Actions.PLAYER_DOWN:
        newState = {...state, y: state.y+1};
        break;
      case Actions.PLAYER_ROTATE_LEFT:
        newState = {...state, r: state.r-1};
        break;
      case Actions.PLAYER_ROTATE_RIGHT:
        newState = {...state, r: state.r+1};
        break;
    }
    if (newState.r < 0) {
      newState.r = 3;
    }
    else if (newState.r > 3) {
      newState.r = 0;
    }
    let newCells = state.piece.getCells(newState.x, newState.y, newState.r);
    const isRotation = action === Actions.PLAYER_ROTATE_LEFT
      || action === Actions.PLAYER_ROTATE_RIGHT;
    const isSideways = action === Actions.PLAYER_RIGHT
      || action === Actions.PLAYER_LEFT;
    if (newCells.some(p => p.x < 0)) {
      if (isRotation) return {...state};
      newState.x++;
    }
    else if (newCells.some(p => p.x >= board.width)) {
      if (isRotation) return {...state};
      newState.x--;
    }
    newCells = state.piece.getCells(newState.x, newState.y, newState.r);
    if (newCells.every(p => board.available(p.x, p.y))) {
      return {...newState};
    }
    else if (isSideways) {
      return {...state};
    }
    else {
      let cells = state.piece.getCells(state.x, state.y, state.r);
      let yToCheck = new Set();
      cells.forEach(p => {
        board.set(p.x, p.y, state.piece.color());
        yToCheck.add(p.y);
      });
      yToCheck = Array.from(yToCheck);
      yToCheck.sort();
      let newPts = 0;
      for (const y of yToCheck) {
        if (board.rowFull(y)) {
          board.deleteRow(y);
          newPts++;
        }
      }
      return {...stateProvider(), points: state.points + newPts};
    }
  }
};
