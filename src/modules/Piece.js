export default class Piece {
  constructor(color, map) {
    this.clr = color;
    this.pieceMap = map;
    this.rotations = [map];

    function rotRight(pieces) {
      return pieces.map((row,r) => {
        return row.map((val,c) => pieces[c][pieces.length-1-r]);
      });
    }

    for (let i = 0; i < 3; i++) {
        this.rotations.push(rotRight(this.rotations[i]));
    }
  }

  color() {
    return this.clr;
  }

  getCells(posX, posY, rot) {
    const cells = [];
    const pieceMap = this.rotations[rot];
    for (let r = 0; r < pieceMap.length; r += 1) {
      let row = pieceMap[r];
      for (let c = 0; c < row.length; c += 1) {
          let cell = row[c];
          if (cell == 1) cells.push({x: posX + c, y: posY + r})
      }
    }
    return cells;
  }
};
